const { roll, score, startGame } = require('../src');
describe('Bowling:', () => {

    beforeEach(() => {
        startGame()
    })

    it('Should return 9 when user roll one time', () => {
        roll(9);
        expect(score()).toEqual(9);
    });

    it('Should return 5 when user roll two times, first time scores 2 and second time scores 3', () => {;
        roll(2);
        roll(3);
        expect(score()).toEqual(5);
    });

    it('Should return 50 when user roll 20 times (full game)', () => {
        for(i = 0; i < 10; i++){
            roll(2);
            roll(3);
        }
        expect(score()).toEqual(50)
    })

    it('Should return 60 when user roll 19 times (full game) with one strike', () => {
        roll(10);
        for(i = 0; i < 9; i++){
            roll(2);
            roll(3);
        }
        expect(score()).toEqual(60)
    })

    it('Should return 77 when user roll 18 times (full game) with two strike', () => {
        roll(10);
        roll(10)
        for(i = 0; i < 8; i++){
            roll(2);
            roll(3);
        }
        expect(score()).toEqual(77)
    })
 
});