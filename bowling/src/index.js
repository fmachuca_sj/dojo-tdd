let globalScore = 0;
let bonusRolls = 0; 

exports.roll = (pinsRolled) => {
    if(bonusRolls > 0){
        globalScore +=pinsRolled;
        bonusRolls--;       
    }

    globalScore +=pinsRolled;

    if(pinsRolled == 10){
        bonusRolls += 2;
    }
}
exports.score = () => {
    return globalScore;
}

exports.startGame = () => {
    globalScore = 0;
}