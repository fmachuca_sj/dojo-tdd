const greet = require('../src')
describe('Index:', () => {
    it('Should greet with name Bob', () => {
        const response = greet('Bob')
        expect(response).toEqual('Hello, Bob.')
    });

    it('Should greet with my friend', () => {
        const response = greet(null)
        expect(response).toEqual('Hello, my friend.')
    });

    it('Should greet witth UPPERS', () => {
        const response = greet("BOB")
        expect(response).toEqual('HELLO BOB!')
    });
 
    it('Should greet witth array', () => {
        const response = greet(['BOB','Jilly'])
        expect(response).toEqual('Hello, BOB and Jilly.')
    });

    it('Should greet witth array 3 3l3m3nts', () => {
        const response = greet(['Amy','Brian', "Charlotte"])
        expect(response).toEqual("Hello, Amy, Brian and Charlotte.")
    });

    it.skip('Should greet witth array and yell the name whit uppercase', () => {
        const response = greet(['Amy','BRAYATHAN', "Charlotte"])
        expect(response).toEqual("Hello, Amy and Charlotte. AND HELLO BRAYATHAN")
    });
});