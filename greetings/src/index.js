const greet = (name) => {
    if(name == null){
        return 'Hello, my friend.'
    } else if(Array.isArray(name)) {
        let gret = 'Hello'
        name.forEach((value, index) => {
            if(index+1 < name.length){
                gret += `, ${value}` ;
            }else{
                gret += ` and ${value}.`
            }
        })
        return gret
    }else{
        if(name===name.toUpperCase()){
            return `HELLO ${name}!`        
        }
        return `Hello, ${name}.`
    }
}

module.exports = greet;