import React from 'react';
import { shallow } from 'enzyme';

import App from '../src/App';
import Todos from '../src/screens/todos/todos'

describe('App:', () => {
  it('Should render', () => {
    const wrapper = shallow(<App />);
    const todos = wrapper.find(Todos);

    expect(todos.length).toEqual(1);
  });
});