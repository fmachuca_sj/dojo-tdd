import React from 'react';
import { View, Text } from 'react-native';
import { shallow } from 'enzyme';

import Todos from '../../../src/screens/todos/todos'

describe('Todos Screen:', () => {
  it('Should render', () => {
    const wrapper = shallow(<Todos />);
    const view = wrapper.find(View).at(0);
    const text = view.find(Text);

    expect(text.length).toEqual(1);
    expect(text.children().text()).toEqual('First Todo');
  });
});