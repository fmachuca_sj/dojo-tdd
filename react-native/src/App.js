import React from 'react';
import { Provider } from 'react-redux';

import Todos from './screens/todos/todos'
import store from './store';

const App = () => (
  <Provider store={store}>
    <Todos />
  </Provider>
);

export default App;