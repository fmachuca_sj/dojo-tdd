const {primeDecomposition} = require('../src/index')

describe('Prime Factors:', () => {
    it('Should return empty prime decomposition list for number 1', () => {
        const number = 1

        let primeDescomposition = primeDecomposition(number) 

        expect(primeDescomposition).toEqual([])
    });

    it('Should return [2] prime decomposition list for number 2', () => {

        const number = 2

        let primeDescomposition = primeDecomposition(number) 

        expect(primeDescomposition).toEqual([2])
    });

    it('Should return [3] prime decomposition list for number 3', () => {

        const number = 3

        let primeDescomposition = primeDecomposition(number) 

        expect(primeDescomposition).toEqual([3])
    });

    it('Should return [2, 2] prime decomposition list for number 4', () => {

        const number = 4

        let primeDescomposition = primeDecomposition(number) 

        expect(primeDescomposition).toEqual([2, 2])
    });

    it('Should return [5] prime decomposition list for number 5', () => {

        const number = 5

        let primeDescomposition = primeDecomposition(number) 

        expect(primeDescomposition).toEqual([5])
    });

    it('Should return [2,3] prime decomposition list for number 6', () => {

        const number = 6

        let primeDescomposition = primeDecomposition(number) 

        expect(primeDescomposition).toEqual([2,3])
    });

});