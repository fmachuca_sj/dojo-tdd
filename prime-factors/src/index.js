
exports.primeDecomposition = function (number) {
    const primes= [2,3,5];

    if (number === 1) {
        return []
    }

    return cycle(primes, number)
}

function cycle(primes, number) {
    let results = []
    let local = number
    primes.forEach(element => {
        if (local % element === 0) {
            results.push(element)
            local = local / element
        }
    });

    return results
} 