exports.add = (input) => {
    if(input === '') {
        return 0;
    }

    const numbersToSum = getNumbers(input);
    const negatives = getNegatives(numbersToSum);

    if(negatives.length > 0){
        throw new Error(`negatives not allowed: ${negatives.toString()}`);
    }
    return numbersToSum.reduce((sum, current)=> sum+=parseInt(current),0)
}

function getNegatives(numberList) {
    return numberList.filter((number)=>parseInt(number)<0);
}

function getNumbers(input) {
    return input.replace(/\n/g,',').split(',')
}