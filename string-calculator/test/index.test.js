const {add} = require('../src');
describe('String Calculator:', () => {
    it('Should return Zero when an empty string is provided', () => {
        expect(add('')).toEqual(0);
    });

    it('Should return 1 when "1" string is provided', () => {
        expect(add('1')).toEqual(1);
    });
    
    it('Should return 12 when "1,11" string is provided', () => {
        expect(add('1,11')).toEqual(12);
    });
    
    it('Should return 8 when "1\n7" string is provided', () => {
        expect(add('1\n7')).toEqual(8);
    });
    
    it('Should return 10 when "1\n2,3\n4" string is provided', () => {
        expect(add('1\n2,3\n4')).toEqual(10);
    });
    it('Should throw an exception when "-1,2,-3" string is provided', () => {
        expect(()=>{add('-1,2,-3')}).toThrowError('negatives not allowed: -1,-3');
    });
});